import { Component, Input } from "@angular/core";

import { ITrack } from "../../../shared/models";
import { LogService } from "../../../core/services";
import { PlayerService } from "../../services/player.service";
import { AuthService } from "../../../core/services/auth.service";
import { DialogService } from "../../../core/services/dialog.service";

@Component({
    moduleId: module.id,
    selector: 'track-list',
    templateUrl: 'track-list.component.html'
})
export class TrackListComponent {

    @Input() tracks: Array<ITrack>;
    
    constructor(
        private authService: AuthService,
        private logService: LogService,
        public playerService: PlayerService,
        private dialogService: DialogService) {

    }

    public record(track: ITrack, usernameAttempt?: string) {
        if (AuthService.CURRENT_USER) {
            this.dialogService.confirm(
                'Are you sure you want to re-record this track?'
            ).then((ok) => {
                if (ok) this._navToRecord(track);
            });
        } else {
            this.authService.promptLogin(
                'Provide and email and password to record.',
                usernameAttempt
            ).then(
                this._navToRecord.bind(this, track),
                (usernameAttempt) => {
                    // initiate sequence again
                    this.record(track, usernameAttempt);
                }
                );
        }
        // this.logService.inspect(track);
    }

    private _navToRecord(track: ITrack) {
        //TODO: navigate to record screen
        this.logService.debug('yes, re-record', track);
    }
}