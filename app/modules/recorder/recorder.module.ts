import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from 'nativescript-angular/router';

import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes } from '@angular/router';

import { SharedModule } from "../shared/shared.module";
import { PROVIDERS } from "./services";
import { RecordComponent } from './components/recorder.component';


const COMPONENTS: any[] = [
    RecordComponent
]
const routes: Routes = [
    {
        path: '',
        component: RecordComponent
    }
];

@NgModule({
    imports: [
        SharedModule,
        NativeScriptRouterModule.forChild(routes)
    ],
    declarations: [ ...COMPONENTS ],
    providers: [...PROVIDERS],
    schemas: [NO_ERRORS_SCHEMA]
})
export class RecorderModule {

}