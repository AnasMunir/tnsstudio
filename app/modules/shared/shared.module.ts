// nativescript
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
// angular
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
// app
import { PIPES } from './pipes';

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule
    ],
    declarations: [
        ...PIPES],
    exports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        ...PIPES
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule { }