import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'action-bar',
    template: 'action-bar.component.html'

})
export class ActionBarComponent {

    @Input() title: string;
    
    constructor() {

    }
}