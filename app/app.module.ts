import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
// import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { CoreModule } from "./modules/core/core.module";
// import { PlayerModule } from "./modules/player/player.module";
import { AppRoutingModule } from "./app.routing";

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpModule } from "nativescript-angular/http";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        CoreModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
